Este proyecto se desarrolla para el curso de **Arquitectura de software** de la universidad [Upla](https://upla.edu.pe/)

## ¿Qué hacer para ver el proyecto?

Para ver el proyecto con los estilos y recursos necesarios se debe de 
instalar primero nodejs para poder ejecutar en un terminal el comando 
para la compilación de los recursos públicos.

cuando se este en la carpeta del proyecto con la terminal se debe de ejecutar

```
npm run dev
```

## Datos del proyecto

- Laravel v8 framework PHP.
- Bootstrap 4 framework CSS.
- Font awesone 5 iconos.

## Datos del curso y estudiante

- Profesor: Arturo Solis Flores
- Estudiante: Julio César Palacios Rojas

## Datos del curso

- HU-01-IS-333171-2015-07-1R-ARQUITECTURA DE SOFTWARE


## Crear la base de datos

```
php artisan migrate:install
```

## Crear registros dentro de las tablas para pruebas
```
php artisan db:seed
```

## Restaurar la base de datos e ingresar datos en las tablas registradas para ello
```
php artisan migrate:refresh --seed
```