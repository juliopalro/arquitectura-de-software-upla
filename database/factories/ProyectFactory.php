<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Illuminate\Support\Str as Str;

$factory->define(App\Project::class, function (Faker $faker) {
    $max = $faker->numberBetween(100, 200);
    $title = $faker->catchPhrase();
    return [
        'title' => $title,
        'url' => Str::slug($title),
        'description' => $faker->text($max),
    ];
});
