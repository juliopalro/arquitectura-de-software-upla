<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendContactMessage;

class ContactController extends Controller
{
    /**
     * Envía el correo para contactarse
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full-name' => 'required',
            'email' => 'required|email:rfc,dns',
            'phone' => 'required',
            'message' => 'required|max: 255'
        ], [
            'full-name.required' => __('Necesito tu nombre completo para saber a quién me dirijo.')
        ]);

        if ($validator->fails()) {
            return redirect('contact')->withErrors($validator)->withInput();
        } else {
            // enviar correo antes de devolver el status
            Mail::to('juliopalro@gmail.com')->queue(new SendContactMessage($request->all()));
            return view('contact')->with('fullName', $request->input('full-name'));
        }
    }
}
