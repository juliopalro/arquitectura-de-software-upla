<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();

        return view('project.index')->with('projects', $projects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form = [
            'action' => route('project.store'),
            'method' => "POST"
        ];

        return view('project.form', compact('form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:projects',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('project/create')->withErrors($validator)->withInput();
        }

        $project = [
            'title' => $request['title'],
            'url' => Str::slug($request['title']),
            'description' => $request['description']
        ];

        $project = Project::create($project);

        return redirect('project/'.$project->url);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($url)
    {
        $project = Project::where('url', $url)->firstOrFail();

        return view('project.show')->with('project', $project);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::findOrFail($id);

        $form = [
            'action' => route('project.update', $project->id),
            'method' => "PUT"
        ];

        return view('project.form', compact('project', 'form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:projects,title,'.$id,
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('project/'.$id.'/edit')->withErrors($validator)->withInput();
        }

        $project = Project::findOrFail($id);

        $project->title = $request['title'];
        $project->url = Str::slug($request['title']);
        $project->description = $request['description'];

        $project->save();

        return redirect('project/'.$project->url);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::findOrFail($id);

        $project->delete();

        return redirect('project');
    }
}
