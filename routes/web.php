<?php

use Illuminate\Support\Facades\Route;

Route::view('/', 'home')->name('home');
Route::view('about', 'about')->name('about');
Route::resource('project', 'ProjectController');

Route::view('contact', 'contact')->name('contact');
Route::post('contact', 'ContactController@store')->name('contact');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
