@extends('layouts.app')

@section('title', 'Inicio')

@section('content')
<div class="text-center py-5 bg-img-pexels-lukas bg-img-size-cover mb-5">
  <img class="img-fluid rounded-circle img-thumbnail bg-light" src="{{ asset('/images/profile.png') }}" alt="">

  <h1 class="my-4 font-size-2x font-weight-bold font-montserrat">Julio César Palacios Rojas</h1>

  <p class="font-weight-bold">Analista de aplicaciones - Desarrollador - UX/UI</p>
</div>
<div class="container">
  <h2 class="text-center border-bottom pb-2 mb-5">Enlaces</h2>
  <div class="row">
    <div class="col-md-4 mb-3">
      <a href="about" class="card bg-primary text-center text-white">
        <div class="card-body">
          <i class="fas fa-user-alt font-size-5x"></i>
          <br>
          <span class="font-size-2x">About</span>
        </div>
      </a>
    </div>
    <div class="col-md-4 mb-3">
      <a href="portafolio" class="card bg-warning text-center text-white">
        <div class="card-body">
          <i class="fas fa-folder-open font-size-5x"></i>
          <br>
          <span class="font-size-2x">Portafolio</span>
        </div>
      </a>
    </div>
    <div class="col-md-4 mb-3">
      <a href="contact" class="card bg-info text-center text-white">
        <div class="card-body">
          <i class="fas fa-id-card font-size-5x"></i>
          <br>
          <span class="font-size-2x">Contact</span>
        </div>
      </a>
    </div>
  </div>
</div>
@endsection
