@extends('layouts.app')

@section('title', 'Portafolio')

@section('content')
<div class="container py-5">
  
  @if ( $project )
    <h2>
      <a class="btn btn-sm btn-secondary" href="{{ url('/project') }}">Proyectos</a> 
      {{ $project->title }} 
      <a class="btn btn-sm btn-primary" href="{{ url('/project/'.$project->id.'/edit') }}">Editar</a> 
      <button type="button" class="btn btn-light btn-sm" data-toggle="modal" data-target="#deleteProject">Eliminar</button>
    </h2>
    <p><b>Descripción:</b> {{ $project->description }}</p>
    <p><b>url:</b> {{ $project->url }}</p>

    <div class="modal fade" id="deleteProject" tabindex="-1" aria-labelledby="deleteProjectLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header text-danger">
            <h5 class="modal-title font-weight-bold" id="exampleModalLabel">¿Está seguro de eliminarlo?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form method="POST" action="{{ url('/project/'.$project->id) }}" id="projectForm">
            <div class="modal-body">
              <p>El proyecto <b>{{ $project->title }}</b> no se podrá recuperar una vez eliminado.</p>
                @method('DELETE')
                @csrf              
            </div>
            <div class="modal-footer">
              <button class="btn btn-outline-danger btn-sm" id="sendMessageButton" type="submit">Eliminar</button>
              <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Cancelar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  @endif

</div>
@endsection
