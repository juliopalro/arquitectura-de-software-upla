@extends('layouts.app')

@section('title', 'Portafolio')

@section('content')
<div class="container py-5">
  <h1 class="mb-3"><i class="fas fa-folder"></i> Portafolio</h1>

  @if ( $projects->count() > 0 )
    <h2 class="mb-3">Proyectos creados: <a class="btn btn-primary btn-sm" href="{{ url("/project/create") }}">Nuevo</a></h2>

    <div class="d-flex justify-content-around flex-wrap">
      <div class="container p-0">
        <div class="row">
        @foreach( $projects as $project )
          <div class="col-md-6 col-lg-4">
            <div class="card mb-3">
              <div class="card-body d-flex justify-content-between">
                <h5 class="card-title font-weight-bold mb-0 text-truncate">{{ $project->title }}</h5>
                <a class="btn btn-primary btn-sm ml-2" href="{{ url("project/{$project->url}") }}">Ver</a>
              </div>
            </div>
          </div>
        @endforeach
        </div>
      </div>
    </div>
  @endif
</div>
@endsection
