@extends('layouts.app')

@section('title', 'Portafolio')

@section('content')
<div class="container py-5">

  <form method="POST" action="{{ $form['action'] }}" id="projectForm">
    @method($form['method'])
    @csrf
    <div class="control-group">
      <div class="form-group floating-label-form-group controls mb-0 pb-2">
        <label for="title">Título</label>
        <input class="form-control" id="title" name="title" type="text" placeholder="Nombre" value="{{ old('title', $project->title ?? null) }}">
        @error('title')
        <p class="help-block text-danger">{{ $message }}</p>
        @enderror
      </div>
    </div>
    <div class="control-group">
      <div class="form-group floating-label-form-group controls mb-0 pb-2">
        <label for="description">Descripción</label>
        <textarea class="form-control" id="description" name="description" rows="5" placeholder="Mensaje">{{ old('description', $project->description ?? null) }}</textarea>
        @error('description')
        <p class="help-block text-danger">{{ $message }}</p>
        @enderror
      </div>
    </div>
    <div class="form-group">
      <button class="btn btn-primary btn-sm" id="createUpdateButton" type="submit">
        <i class="fas fa-save"></i> {{ ($form['method'] == 'POST')? 'Crear':'Actualizar' }}
      </button>
      <a class="btn btn-sm btn-secondary" href="{{ url()->previous() }}">Cancelar</a> 
    </div>
  </form>

</div>
@endsection
