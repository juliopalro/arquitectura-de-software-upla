<!DOCTYPE html>
<html lang="es">
<head>
	<title>Nuevo mensaje desde contactame</title>
	<style>
		.text-primary {
			color: #007bff;
		}
		.text-dark {
			color: #343a40;
		}
		.text-secondary {
			color: #6c757d;
		}
	</style>
</head>
<body>
	<p>Recibiste un nuevo mensaje de contactame de: <b class="text-primary">{{ $msg['full-name'] }}</b>.</p>
	<p><b>Correo: </b> <span class="text-primary">{{ $msg['email'] }}</span>.</p>
	<p><b>Teléfono: </b> <span class="text-primary">{{ $msg['phone'] }}</span>.</p>

	<p><b>Asunto: </b><span class="text-dark">{{ $msg['message'] }}</span></p>

	<hr>

	<p><small class="text-secondary">Mensaje automático desde la web al rellenar y enviarl el formulario de contactame.</small></p>
</body>
</html>

