<nav class="navbar navbar-expand-md navbar-dark bd-navbar bg-dark">
  <a class="navbar-brand text-center" href="{{ url('/') }}">Juliopalro</a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menuBar" aria-controls="menuBar" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="menuBar">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item{{ (\Request::route()->getName() == 'home') ? ' active bg-secondary' : '' }}">
        <a class="nav-link" href="{{ url('/') }}">{{ __('Home') }} <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item{{ (\Request::route()->getName() == 'about') ? ' active bg-secondary' : '' }}">
        <a class="nav-link" href="{{ url('/about') }}">{{ __('About') }}</a>
      </li>
      <li class="nav-item{{ (\Request::route()->getName() == 'project') ? ' active bg-secondary' : '' }}">
        <a class="nav-link" href="{{ url('project') }}">{{ __('Projects') }}</a>
      </li>
      <li class="nav-item{{ (\Request::route()->getName() == 'contact') ? ' active bg-secondary' : '' }}">
        <a class="nav-link" href="{{ url('contact') }}">{{ __('Contact') }}</a>
      </li>
    </ul>
    @include('layouts.menu-sesion')
  </div>
</nav>
