@extends('layouts.app')

@section('title', 'About')

@section('content')
<div class="container-fluid">
  <div class="row d-flex align-items-center text-center text-md-left py-5">
    <div class="col-md-5 text-center">
      <img class="img-fluid rounded-circle img-thumbnail" src="{{ asset('/images/profile.png') }}" alt="">
    </div>
    <div class="col-md-7 pl">
      <h1 class="mb-3 font-size-4x font-weight-bold font-montserrat">Julio C. Palacios Rojas</h1>
      <div class="row mb-3">
        <div class="col-md-3">
          <p class="mb-0"><small>Código</small></p>
          <p class="font-weight-bold">J08629E</p>
        </div>
        <div class="col-md-4">
          <p class="mb-0"><small>Phone</small></p>
          <p class="font-weight-bold"><small>+51</small> 915203458</p>
        </div>
        <div class="col-md-5">
          <p class="mb-0"><small>Correo Personal</small></p>
          <p class="font-weight-bold">juliopalro<small>@gmail.com</small></p>
        </div>
      </div>
      <div class="row mb-3">
        <div class="col-md-4">
          <a class="btn btn-outline-primary" href="https://www.facebook.com/juliopalro" target="_blanck"><i class="fab fa-facebook-f"></i> facebook</a>
        </div>
      </div>

      <p>Estudiante de la <b>Universidad Peruana Los Andes</b>.</p>

    </div>
  </div>
  <div class="container pt-4">
    <h2 class="text-center border-bottom mb-5 pb-2">Habilidades/Características</h2>
    <div class="row">
      <div class="col-md-4 mb-2">
        <div class="card">
          <img class="card-img-top" src="https://picsum.photos/id/1/250/150" alt="">
          <div class="card-body">
            <p class="card-text">Programación en varios lenguajes.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4 mb-2">
        <div class="card">
          <img class="card-img-top" src="https://picsum.photos/id/103/250/150" alt="">
          <div class="card-body">
            <p class="card-text">Viajero.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4 mb-2">
        <div class="card">
          <img class="card-img-top" src="https://picsum.photos/id/180/250/150" alt="">
          <div class="card-body">
            <p class="card-text">Autodidacta.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
