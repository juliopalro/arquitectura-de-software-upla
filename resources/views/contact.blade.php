@extends('layouts.app')

@section('title', 'Contact')

@section('content')
  <div class="container py-5">
    <h2 class="border-bottom border-primary text-center pb-2 mb-5">Contactame</h2>
    {{-- <?php dd(session) ?> --}}
    @isset($fullName)
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <strong><i class="fas fa-check-circle"></i>{{ $fullName }} gracias por comunicarte conmigo</strong>, te estaré respondiendo en cuanto me sea posible.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endif

    @if($errors->any())
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong><i class="fas fa-exclamation-triangle"></i> Formulario incompleto</strong>, para poder comunicarme contigo porfavor rellena los campos necesarios.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    @endif

    <form method="POST" action="{{ route('contact') }}" id="contactForm" name="sentMessage">
        @csrf
        <div class="control-group">
          <div class="form-group floating-label-form-group controls mb-0 pb-2">
            <label for="full-name">Nombre completo</label>
            <input class="form-control" id="full-name" name="full-name" type="text" placeholder="Nombre" value="{{ old('full-name') }}">
            @error('full-name')
              <p class="help-block text-danger">{{ $message }}</p>
            @enderror
          </div>
        </div>
        <div class="control-group">
          <div class="form-group floating-label-form-group controls mb-0 pb-2">
            <label for="email">Correo electrónico</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Correo" value="{{ old('email') }}">
            @error('email')
              <p class="help-block text-danger">{{ $message }}</p>
            @enderror
          </div>
        </div>
        <div class="control-group">
          <div class="form-group floating-label-form-group controls mb-0 pb-2">
            <label for="phone">Número telefónico</label>
            <input class="form-control" id="phone" name="phone" type="tel" placeholder="Número telefónico" value="{{ old('phone') }}">
            @error('phone')
              <p class="help-block text-danger">{{ $message }}</p>
            @enderror
          </div>
        </div>
        <div class="control-group">
          <div class="form-group floating-label-form-group controls mb-0 pb-2">
            <label for="message">Mensaje</label>
            <textarea class="form-control" id="message" name="message" rows="5" placeholder="Mensaje">{{ old('message') }}</textarea>
            @error('message')
              <p class="help-block text-danger">{{ $message }}</p>
            @enderror
          </div>
        </div>
        <div class="form-group">
          <button class="btn btn-primary btn-xl" id="sendMessageButton" type="submit">Contactar</button>
        </div>
    </form>
  </div>
@endsection
